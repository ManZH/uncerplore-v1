/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.domain;

public enum NewlyDiscoverType {
	AndCombine,
	UnknwonExploreKnT_FC,
	UnknwonExploreKnT_RPWK,
	UnknwonExploreKnT_NRPWK,
	
	UnknwonExploreUKnT_FC,
	UnknwonExploreUKnT_WK,
	
	UnknownTag
}
