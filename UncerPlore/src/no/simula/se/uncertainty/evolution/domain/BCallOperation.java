/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.domain;

public class BCallOperation extends BOperation{
	
	private static final long serialVersionUID = -8834321717872551868L;

	public BCallOperation(String opName, Class<?>... pars){
		setName(opName);
		setOpName(opName);
		setPars(pars);
	}
	
	public BCallOperation(String guard,String instance, String opName, Class<?>... pars){
		this(opName, pars);
		setGuard(new BConstraint(guard, instance, ConstraintContainerType.Guard));
	}
}
