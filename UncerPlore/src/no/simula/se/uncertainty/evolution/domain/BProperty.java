/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.domain;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import no.simula.se.uncertainty.evolution.rules.DiscoverUncertainWorldProblem;

public class BProperty extends BElement {
	
	private static final long serialVersionUID = 1L;
	
	public BProperty(){}
	
	public BProperty(String name){
		this.setName(name);
	}
	
	private Set<String> consfragements;
	private String instance_name;


	public Set<String> getConsfragements() {
		if(consfragements == null) consfragements = new HashSet<String>();
		return consfragements;
	}

	public void setConsfragements(Set<String> consfragements) {
		this.consfragements = consfragements;
	}
	
	public String getInstance_name() {
		return instance_name;
	}

	public void setInstance_name(String instance_name) {
		this.instance_name = instance_name;
	}
	
	public EObject getInstance() {
		return DiscoverUncertainWorldProblem.world.getInstance(instance_name);
	}
}
