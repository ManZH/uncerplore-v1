/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.domain;

public enum OccurOfIndS {
	NoneInds, //O0 none of indeterminacy source occurred
	SpecifiedInds, //O1 only specified of indeterminacy source occurred
	OtherInds, //O2 specified of indeterminacy did not occurred and at least one of other indeterminacy source occurred.
	Combined, //O3 specified of indeterminacy source and at least one of other indeterminacy source occurred

}
