/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.domain;

import java.util.HashSet;
import java.util.Set;

public class BPropertyConstraintGroup {
	private String property;
	private String instance_name;
	private Set<String> consFrags;
	
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getInstance_name() {
		return instance_name;
	}
	public void setInstance_name(String instance_name) {
		this.instance_name = instance_name;
	}
	public Set<String> getConsFrags() {
		if(consFrags == null) consFrags = new HashSet<String>();
		return consFrags;
	}
	public void setConsFrags(Set<String> consFrags) {
		this.consFrags = consFrags;
	}
	
}
