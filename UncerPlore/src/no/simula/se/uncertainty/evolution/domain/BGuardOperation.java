/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.domain;

public class BGuardOperation extends BOperation{
	
	private static final long serialVersionUID = -8834321717872551868L;

	
	public BGuardOperation(String guard,String instance){
		setGuard(new BConstraint(guard, instance, ConstraintContainerType.Guard));
		this.setName(guard);
		this.setOpName("noAction");
	}
	
	public String execute(){
		count1Visit();
		//System.err.println("no operation contains:"+this.getName());
		return getName();
	}
}
