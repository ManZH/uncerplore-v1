/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.rules;

import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.Program;
import org.moeaframework.problem.AbstractProblem;
import org.moeaframework.util.tree.Environment;
import org.moeaframework.util.tree.IfElse;
import org.moeaframework.util.tree.Rules;
import org.moeaframework.util.tree.Sequence;

import no.simula.se.uncertainty.evolution.cs.CSUtil;
import no.simula.se.uncertainty.evolution.domain.BModelHandler;
import no.simula.se.uncertainty.evolution.domain.UncertainWorld;
import no.simula.se.uncertainty.evolution.rules.action.T1;
import no.simula.se.uncertainty.evolution.rules.action.T2;
import no.simula.se.uncertainty.evolution.rules.action.T4;
import no.simula.se.uncertainty.evolution.rules.condition.BranchCoverageTR;
import no.simula.se.uncertainty.evolution.rules.condition.BranchPossibleNext;
import no.simula.se.uncertainty.evolution.rules.condition.isCurrentStateNew;

public class DiscoverUncertainWorldProblem extends AbstractProblem {
	
	public static boolean first = true;

	private final Rules rules;

	public static UncertainWorld world;
	public static CSUtil cs;

	public DiscoverUncertainWorldProblem(UncertainWorld register) {
		super(1, 1);
		
		rules = new Rules();
		//rules.add(new isBlock());
//		rules.add(new TransitLessExecuted());
//		rules.add(new TransitExcludedOpNotNull());
//		
//		rules.add(new isCoverageState100());
//		rules.add(new isCoverageState80());
//		rules.add(new isCoverageState60());
//		rules.add(new isCoverageState40());
//		rules.add(new isCoverageState20());
//		
//		rules.add(new isCoverageTR100());
//		rules.add(new isCoverageTR80());
//		rules.add(new isCoverageTR60());
//		rules.add(new isCoverageTR40());
//		rules.add(new isCoverageTR20());
//		
//		rules.add(new IfElse(Void.class));
//		rules.add(new Sequence(Void.class, Void.class));
		
//		rules.add(new T1I1());
//		rules.add(new T1I2());
//		rules.add(new T1I3());
//		rules.add(new T1I4());
//		rules.add(new T2I1());
//		rules.add(new T2I2());
//		rules.add(new T2I3());
//		rules.add(new T2I4());
//		rules.add(new T3I1());
//		rules.add(new T3I2());
//		rules.add(new T3I3());
//		rules.add(new T3I4());
		
		rules.add(new T1());
		rules.add(new T2());
		rules.add(new T4());
		rules.add(new isCurrentStateNew());
		rules.add(new IfElse(Void.class));
		rules.add(new BranchCoverageTR());
		rules.add(new BranchPossibleNext());
		rules.add(new Sequence(Void.class, Void.class));
		rules.setReturnType(Void.class);

//		rules.add(new Random());
		world = register;
		cs = world.cs;
	}
//	
//	public DiscoverUncertainWorld(UncertainWorld world) {
//		this();
//		
//	}

	@Override
	public synchronized void evaluate(Solution solution) {
		
		Program program = (Program)solution.getVariable(0);

		world.reset();
		
		while (world.getRemainingSteps() > 0 && !world.isTerminate()) {
			//System.out.println(beliefMap.getRemainingSteps());
			Environment environment = new Environment();
			environment.set("world", world);
			program.evaluate(environment);
		}
		System.err.println(world.isTerminate());
		double fit = world.getFitness();
		world.getCurrent().setFitness(fit);
		world.getCurrent().setSolution(program.toString());
		world.cs.stopSUT();
		world.getCurrent().setExpName(world.cs.getCsName());
		displayLastEvaluation();
		BModelHandler.saveBModelDate("log", world.getCurrent(), first); // save the archive
		first = false;
		solution.setObjective(0, fit);
		
	}
	
	public void displayLastEvaluation() {
		world.display();
	}

	@Override
	public Solution newSolution() {
		Solution solution = new Solution(1, 1);
		solution.setVariable(0, new Program(rules));
		return solution;
	}
}
