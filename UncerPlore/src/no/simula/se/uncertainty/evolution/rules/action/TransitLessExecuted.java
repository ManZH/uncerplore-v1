/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.rules.action;

import org.moeaframework.util.tree.Environment;
import org.moeaframework.util.tree.Node;

import no.simula.se.uncertainty.evolution.domain.TransitionOption;
import no.simula.se.uncertainty.evolution.domain.UncertainWorld;
import no.simula.se.uncertainty.evolution.util.DiscoverUncertaintyException;

public class TransitLessExecuted extends Node{

	public TransitLessExecuted(){
		super();
	}
	
	@Override
	public Node copyNode() {
		return new TransitLessExecuted();
	}

	@Override
	public Object evaluate(Environment environment) {
		System.err.println("executing change option");
		UncertainWorld map = environment.get(UncertainWorld.class, "world");
		try {
			map.transit(TransitionOption.LessExecuted, null);
		} catch (DiscoverUncertaintyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
