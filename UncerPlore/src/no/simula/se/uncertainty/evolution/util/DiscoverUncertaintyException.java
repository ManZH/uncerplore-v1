/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.util;

public class DiscoverUncertaintyException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DiscoverUncertaintyException(){
		super();
	}
	
	public DiscoverUncertaintyException(String info){
		super(info);
	}

}
