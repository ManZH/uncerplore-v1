/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.solution;

import java.util.ArrayList;
import java.util.List;

import org.moeaframework.core.variable.Program;
import org.moeaframework.util.tree.Rules;

@SuppressWarnings("serial")
public class DiscoverProgram extends Program {
	
	
	private List<String> newTransition;
	private List<String> newStates;

	public DiscoverProgram(Rules rules) {
		super(rules);
	}

	public List<String> getNewTransition() {
		return newTransition;
	}

	public void setNewTransition(List<String> newTransition) {
		if(this.newTransition == null) this.newTransition = new ArrayList<String>();
		this.newTransition.clear();
		for(String nTR : newTransition){
			this.newTransition.add(nTR);
		}
	}

	public List<String> getNewStates() {
		return newStates;
	}

	public void setNewStates(List<String> newStates) {
		if(this.newStates == null) this.newStates = new ArrayList<String>();
		this.newStates.clear();
		for(String nSt : newStates){
			this.newStates.add(nSt);
		}
	}
	
	public String showTRs(){
		String result = "";
		for(String tr : this.newTransition){
			result = result + tr+"\n";
		}
		return result;
	}
	
	@Override
	public DiscoverProgram copy() {
		return (DiscoverProgram)copyTree();
	}

	@Override
	public DiscoverProgram copyNode() {
		return new DiscoverProgram(getRules());
	}
}
