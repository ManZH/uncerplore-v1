/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import no.simula.se.uncertainty.evolution.domain.BModel;
import no.simula.se.uncertainty.evolution.domain.BOperation;
import no.simula.se.uncertainty.evolution.domain.BState;
import no.simula.se.uncertainty.evolution.domain.BUncertainty;

public class EvaluationBModel {

	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, IOException {
		
		String modelfile = ".json";
		GsonBuilder  bulider = new GsonBuilder();
		bulider.registerTypeAdapter(BOperation.class, new BElementAdapter<BOperation>());

		Gson gson = bulider.create();
		BModel bmodel = gson.fromJson(Files.newBufferedReader(Paths.get(modelfile)), BModel.class);
		System.out.println(bmodel.getUns().size());
		
		System.err.println("=========state===========");
		for(BState bst : bmodel.sts.values()){
			if(!bst.isPreDef()) System.err.println(bst.getName());
			
		}
		
		System.err.println("=========operation===========");
		for(BOperation bop : bmodel.ops.values()){
			System.err.println(bop.getName());
		}
		
		System.err.println("=========uncertainty===========");
		for(BUncertainty bun : bmodel.uns.values()){
			if(!bun.isPreDef()){
				System.err.print(bun.getDegree()+" ");
				if(bun.getSs() != null) System.err.print("ss:"+bun.getSs().getName()+"\n");
				else System.err.print("****[no source]");
				if(bun.getOp() != null) System.err.print("op:"+bun.getOp().getName()+"\n");
				else System.err.print("****[no operation]");
				if(bun.getTs() != null) System.err.print("ts:"+bun.getTs().getName()+"\n");
				else System.err.print("****[no target]");
			}
			
		}
		

	}

}
