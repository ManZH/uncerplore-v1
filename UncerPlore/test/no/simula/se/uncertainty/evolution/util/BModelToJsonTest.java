/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution.util;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.common.util.UML2Util.EObjectMatcher;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLUtil;

import com.google.gson.Gson;

import no.simula.se.uncertainty.evolution.domain.BModelHandler;

public class BModelToJsonTest {
	public static void main(String[] args){

		
		String path = "";
		String sm_name = "";
		String[] setups = {""};
		String saved ="";
		
		org.eclipse.uml2.uml.Package model = (org.eclipse.uml2.uml.Package) ModelUtility.loadUMLModel(path);
		StateMachine sm = (StateMachine) UMLUtil.findEObject(model.allOwnedElements(), new EObjectMatcher(){

			public boolean matches(EObject eObject) {
				return (eObject instanceof StateMachine) && ((StateMachine)eObject).getName().equals(sm_name);
			}});
		org.eclipse.uml2.uml.Package[] configs = ModelUtility.generateSpecifiedTestConfigs(model, setups);
		
		try {

			BModelHandler gen = new BModelHandler();
			gen.generateBModel(sm, configs);
			gen.bmodel.processLinkStates();
			gen.bmodel.findInitalState();

			Gson gson = new Gson();
			try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(saved+".json"))) {
			    writer.write(gson.toJson(gen.bmodel));
			}

		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
