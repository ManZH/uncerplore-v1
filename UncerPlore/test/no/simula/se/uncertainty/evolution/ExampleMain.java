/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.uncertainty.evolution;

import java.util.Properties;

import org.moeaframework.core.Algorithm;
import org.moeaframework.core.Solution;
import org.moeaframework.core.spi.AlgorithmFactory;

import no.simula.se.uncertainty.evolution.domain.BModel;
import no.simula.se.uncertainty.evolution.domain.BModelHandler;
import no.simula.se.uncertainty.evolution.domain.UncertainWorld;
import no.simula.se.uncertainty.evolution.rules.DiscoverUncertainWorldProblem;

public class ExampleMain {
	public static void main(String[] args) {
		try {
			
			String path = ".json";
			String folder = "";
			String fitnessFile ="";
			UncertainWorld world = null;// implement your uncertainty world for setup of system execution and configuration of actions and condition defined in the UncerPlore.
			world.cs = null;// implement your adaption layer of your case study in order to trigger online execution.
			
			BModel bmodel = BModelHandler.loadBModel(path);
			world.setOrigin(bmodel);
			DiscoverUncertainWorldProblem prob = new DiscoverUncertainWorldProblem(world);
			
			//parameter setting
			int generation = 0;
			int maxGenerations = 100;
			Algorithm algorithm = null;
			Properties properties = new Properties();
			properties.setProperty("populationSize", "100");
			world.setMaxSteps(100);
			StringBuilder sb = new StringBuilder();
			
			try {
				algorithm = AlgorithmFactory.getInstance().getAlgorithm(
						"GA", properties, prob);
				while ((generation < maxGenerations) ) {
					bmodel.setGeneration(generation);
					algorithm.step();
					generation++;
					Solution result = algorithm.getResult().get(0);
					sb.append(result.getObjectives()[0]+"\t"+result.getVariable(0).toString()+"\n");
				}
				BModelHandler.saveFitAndSol(folder,fitnessFile,sb.toString());
			} finally {
				if (algorithm != null) {
					algorithm.terminate();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
