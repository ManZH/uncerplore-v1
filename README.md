# UncerPlore: an uncertainty-wise tool for model evolution using genetic programming #

UncerPlore is a tool for belief state machine evolution, which includes two parts: 
	1) discovering new uncertainties and new associations between uncertainty indeterminacy sources with Genetic Programing—one of search algorithms,
	2) updating the objective uncertainty measurements for the known uncertainties and newly discovered uncertainties
	
### Dependency ###

* [MOEA Framework](http://moeaframework.org/) 
* [How to process OCL Abstract Syntax Trees](https://www.eclipse.org/articles/Article-HowToProcessOCLAbstractSyntaxTrees/index.html).
* [gson](https://github.com/google/gson) and [jdom](http://www.jdom.org/)
